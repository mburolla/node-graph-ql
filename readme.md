# Apollo GraphQL Server
A simple GraphQL API.

# Links
[GraphQL](https://www.apollographql.com/blog/graphql/examples/building-a-graphql-api/)

# Running the Server
- Create Apollo account: `https://studio.apollographql.com/dev`
- `node index.js`

# Notes
- Built with IntelliJ IDEA 2021.2.2 (Community Edition)
- Node 14
- Get-Process -Id (Get-NetTCPConnection -LocalPort 4000).OwningProcess
